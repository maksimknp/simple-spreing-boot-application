package edu.netcracker.application.controller;

import edu.netcracker.application.player.MusicPlayer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/music")
public class MusicController {

    @Autowired
    private MusicPlayer musicPlayer;

    @GetMapping("/song/{name}")
    public String getSong(@PathVariable("name") String name){
        return name + ": " + musicPlayer.playSong();
    }
}
