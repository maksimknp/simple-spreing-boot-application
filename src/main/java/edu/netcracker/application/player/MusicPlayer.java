package edu.netcracker.application.player;

import edu.netcracker.application.musicModel.Music;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MusicPlayer {

    @Value("${musicPlayer.volume}")
    private int volume;

    private Music music;

    @Autowired
    public MusicPlayer(@Qualifier("rockMusic") Music music) {
        this.music = music;
    }

    public String playSong(){
        return music.getSong() + " volume: " + volume;
    }
}
