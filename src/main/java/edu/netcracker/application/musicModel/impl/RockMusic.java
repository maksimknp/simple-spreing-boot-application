package edu.netcracker.application.musicModel.impl;

import edu.netcracker.application.musicModel.Music;
import org.springframework.stereotype.Component;

@Component
public class RockMusic implements Music {
    public String getSong() {
        return "Some Rock song";
    }
}
