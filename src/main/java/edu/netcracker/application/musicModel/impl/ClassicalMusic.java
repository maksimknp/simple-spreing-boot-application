package edu.netcracker.application.musicModel.impl;

import edu.netcracker.application.musicModel.Music;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class ClassicalMusic implements Music {
    public String getSong() {
        return "Some classical song";
    }
}
